from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Group, Member


id_member = 1
id_group = 1

lst_group = []


class BasePostGroupAPIView(APIView):
    def post(self, request):
        """/group"""

        if 'name' not in request.data:
            return Response(status=400)
        name = request.data['name']

        description = None
        if 'description' in request.data:
            description = request.data['description']

        global id_group
        group = Group(id_group, name, description)
        id_group += 1

        lst_group.append(group)

        return Response(
            {'response': group.id},
            status=201
        )


class BaseGetGroupSAPIView(APIView):
    def get(self, request):
        """/groups"""

        response = []
        for action in lst_group:
            response.append(
                {
                    'id': action.id,
                    'name': action.name,
                    'description': action.description,
                }
            )

        return Response(
            {'response': response},
            status=200
        )


class IdGroupAPIView(APIView):

    def get(self, request, id):
        """/group/{id}"""

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == id:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        participants = []
        for member in lst_group[i].participants:
            if member.recipient:
                recipient = {
                    'id': member.recipient.id,
                    'name': member.recipient.name,
                    'wish': member.recipient.wish,
                }
            else:
                recipient = {}

            participants.append(
                {
                    'id': member.id,
                    'name': member.name,
                    'wish': member.wish,
                    'recipient': recipient
                }
            )

        response = {
            'id': lst_group[idx].id,
            'name': lst_group[idx].name,
            'description': lst_group[idx].description,
            'participants': participants,
        }

        return Response(response, status=200)

    def put(self, request, id):
        """/group/{id}"""

        if 'name' not in request.data:
            return Response(status=400)

        name = request.data['name']
        description = None
        if 'description' in request.data:
            description = request.data['description']

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == id:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        lst_group[idx].name = name
        lst_group[idx].description = description

        return Response(status=200)

    def delete(self, request, id):
        """/group/{id}"""

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == id:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        lst_group.pop(idx)

        return Response(status=200)


class ParticipantGroupAPIView(APIView):
    def post(self, request, id):
        """/group/{id}/participant"""

        if 'name' not in request.data:
            return Response(status=400)
        name = request.data['name']

        wish = None
        if 'wish' in request.data:
            wish = request.data['wish']

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == id:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        global id_member
        member = Member(id_member, name, wish)
        id_member += 1

        lst_group[idx].participants.append(member)

        return Response({'response': member.id}, status=201)


class GroupIdParticipantIdGroupAPIView(APIView):
    def delete(self, request, groupId, participantId):
        """/group/{groupId}/participant/{participantId}"""

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == groupId:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        idx_member = None
        for i in range(len(lst_group[idx].participants)):
            if lst_group[idx].participants[i].id == participantId:
                idx_member = i
                break
        if idx_member is None:
            return Response(status=404)
        
        lst_group[i].participants.pop(idx_member)

        return Response(status=200)


class TossGroupIdAPIView(APIView):
    def post(self, request, id):
        """/group/{id}/toss"""

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == id:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        if len(lst_group[idx].participants) < 3:
            return Response(status=409)
        
        for i in range(len(lst_group[idx].participants)-1):
            lst_group[idx].participants[i].recipient = lst_group[idx].participants[i+1]
        lst_group[idx].participants[-1].recipient = lst_group[idx].participants[0]

        participants = []
        for member in lst_group[idx].participants:
            participants.append(
                {
                    'id': member.id,
                    'name': member.name,
                    'wish': member.wish,
                    'recipient': {
                        'id': member.recipient.id,
                        'name': member.recipient.name,
                        'wish': member.recipient.wish,
                    }
                }
            )

        response = {
            'id': lst_group[idx].id,
            'name': lst_group[idx].name,
            'description': lst_group[idx].description,
            'participants': participants,
        }

        return Response(response, status=200)


class GroupIdParticipantIdAPIView(APIView):
    def get(self, request, groupId, participantId):
        """/group/{groupId}/participant/{participantId}/recipient"""

        idx = None
        for i in range(len(lst_group)):
            if lst_group[i].id == groupId:
                idx = i
                break
        if idx is None:
            return Response(status=404)

        if len(lst_group[i].participants) <= participantId:
            return Response(status=404)
        
        member = lst_group[i].participants[participantId]
        response = {
            'id': member.id,
            'name': member.name,
            'wish': member.wish
        }

        return Response(response, status=200)
