class Group:
    
    def __init__(self, id, name, description=None, participants=[]):
        self.id = id
        self.name = name
        self.description = description 
        self.participants = participants


class Member:

    def __init__(self, id, name, wish=None, recipient=None):
        self.id = id
        self.name = name
        self.wish = wish
        self.recipient = recipient
