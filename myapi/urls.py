from django.urls import path
from . import views


urlpatterns = [
    path('group/', views.BasePostGroupAPIView.as_view()),
    path('groups/', views.BaseGetGroupSAPIView.as_view()),
    path('group/<int:id>', views.IdGroupAPIView.as_view()),
    path('group/<int:id>/participant', views.ParticipantGroupAPIView.as_view()),
    path('group/<int:groupId>/participant/<int:participantId>', views.GroupIdParticipantIdGroupAPIView.as_view()),
    path('group/<int:id>/toss', views.TossGroupIdAPIView.as_view()),
    path('group/<int:groupId>/participant/<int:participantId>/recipient', views.GroupIdParticipantIdAPIView.as_view()),
]
